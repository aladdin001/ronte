package tasks

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"ronte/app"
	"strings"
	"time"
)

const chunkSize int = 10 // quantity of synchronous get requests

func CheckURL() {
	path := os.Getenv("API_FILES_PATH")
	file, err := os.Open(path + "sites.txt")
	if err != nil {
		log.Println(err.Error())
	}

	content, err := ioutil.ReadAll(file)
	if err != nil {
		log.Println(err)
	}

	c := strings.Replace(string(content), "\r", "", -1) // get rid of windows [13] symbol if necessary
	lines := strings.Split(c, "\n")

	query := `INSERT INTO statistics (site, response_time, response_status) VALUES (?, ?, ?);`

	type Stat struct {
		Site           string
		ResponseTime   time.Duration
		ResponseStatus int
	}
	ch := make(chan Stat, chunkSize) // buffered

	for _, v := range lines {
		go func(v string) {
			now := time.Now()
			res, err := http.Get("http://" + v)
			if err != nil {
				log.Println(err)
			}
			rt := time.Now().Sub(now)

			var status int
			if res != nil {
				status = res.StatusCode
			}
			ch <- Stat{Site: v, ResponseTime: rt, ResponseStatus: status}
		}(v)
	}

	for {
		select {
		case r := <-ch:
			err = app.DB.Exec(query, r.Site, r.ResponseTime, r.ResponseStatus).Error
			if err != nil {
				log.Println(err)
			}
		}
	}
}
