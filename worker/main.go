package main

import (
	"github.com/jasonlvhit/gocron"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/sirupsen/logrus"
	"os"
	"os/signal"
	"ronte/app"
	"ronte/worker/tasks"
	"sync"
	"syscall"
)

func openConnection() {

	var err error
	app.DB, err = gorm.Open("postgres", os.Getenv("API_DB_OPEN"))
	if err != nil {
		panic("failed to connect database:\n" + err.Error())
	}

	err = app.DB.DB().Ping()
	if err != nil {
		panic("failed to connect database:\n" + err.Error())
	}
}

func initLogger() {
	app.Logger = logrus.New()
}

func init() {
	openConnection()
	initLogger()
}

func main() {

	wg := &sync.WaitGroup{}

	// Tasks
	setupTasks(wg)

	defer app.DB.Close()

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	gocronDoneChan := gocron.Start()

	<-signalChan

	app.Logger.Infof("Stopping...")
	gocronDoneChan <- true
	wg.Wait()

	app.Logger.Infof("Done workers")
}

func setupTasks(wg *sync.WaitGroup) {
	gocron.Every(5).Seconds().Do(CronJobGracefullWrapper, wg, tasks.CheckURL)
}

func CronJobGracefullWrapper(wg *sync.WaitGroup, job func()) {
	wg.Add(1)
	job()
	wg.Done()
}
