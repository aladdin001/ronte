package main

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/shopspring/decimal"
	"net/http"
	"os"
	"ronte/app"
	"time"
)

func openConnection() {

	var err error
	app.DB, err = gorm.Open("postgres", os.Getenv("API_DB_OPEN"))
	if err != nil {
		panic("failed to connect database:\n" + err.Error())
	}

	err = app.DB.DB().Ping()
	if err != nil {
		panic("failed to connect database:\n" + err.Error())
	}
}

func init() {
	openConnection()
}

func main() {

	router := gin.Default()
	router.GET("/responsetime/:site", siteResponseTime)
	router.GET("/min_response_time", minResponseTime)
	router.GET("/max_response_time", maxResponseTime)

	s := &http.Server{
		Addr:         "localhost:8080",
		Handler:      router,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	s.ListenAndServe()
}

func siteResponseTime(c *gin.Context) {

	var responseError struct {
		Error string `json:"error"`
	}
	var responseSuccess struct {
		Site         string          `json:"site"`
		ResponseTime decimal.Decimal `json:"average_response_time"`
	}

	site, _ := c.Params.Get("site")

	q := `SELECT AVG(response_time) AS response_time
		FROM
			statistics
		WHERE 
			response_status = 200
			AND site=?;`

	res := decimal.Zero
	err := app.DB.Raw(q, site).Row().Scan(&res)
	if err != nil {
		responseError.Error = err.Error()
		c.JSON(http.StatusBadRequest, responseError)
		return
	}

	responseSuccess.ResponseTime = res
	c.JSON(http.StatusOK, responseSuccess)
}

func minResponseTime(c *gin.Context) {

	var responseError struct {
		Error string `json:"error"`
	}
	var responseSuccess struct {
		Site         string          `json:"site"`
		ResponseTime decimal.Decimal `json:"response_time"`
	}

	var site string

	q := `SELECT site, response_time FROM statistics WHERE response_time = (
			SELECT 
				MIN(response_time) AS response_time
			FROM
     			statistics
			WHERE
    			response_status = 200);`

	res := decimal.Zero
	err := app.DB.Raw(q).Row().Scan(&site, &res)
	if err != nil {
		responseError.Error = err.Error()
		c.JSON(http.StatusBadRequest, responseError)
		return
	}

	responseSuccess.Site = site
	responseSuccess.ResponseTime = res
	c.JSON(http.StatusOK, responseSuccess)
}

func maxResponseTime(c *gin.Context) {

	var responseError struct {
		Error string `json:"error"`
	}
	var responseSuccess struct {
		Site         string          `json:"site"`
		ResponseTime decimal.Decimal `json:"response_time"`
	}

	var site string

	q := `SELECT site, response_time FROM statistics WHERE response_time = (
			SELECT 
				MAX(response_time) AS response_time
			FROM
     			statistics
			WHERE
    			response_status = 200);`

	res := decimal.Zero
	err := app.DB.Raw(q).Row().Scan(&site, &res)
	if err != nil {
		responseError.Error = err.Error()
		c.JSON(http.StatusBadRequest, responseError)
		return
	}

	responseSuccess.Site = site
	responseSuccess.ResponseTime = res
	c.JSON(http.StatusOK, responseSuccess)
}
