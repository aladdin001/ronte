
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

create table statistics
(
  id              serial not null,
  site            varchar(256),
  response_time   numeric,
  response_status integer,
  created         timestamp default CURRENT_TIMESTAMP
);

create index statistics_site_index
  on statistics (site);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

drop index statistics_site_index;
drop table statistics;